FROM abaez/luarocks:latest

RUN apk update
RUN apk --no-cache upgrade
RUN luarocks install luafilesystem
RUN luarocks install luasec
RUN luarocks install luajson

COPY src/libUS.lua libUS.lua
COPY src/updatesecret.lua updatesecret.lua
COPY share/config.lua config.lua

ENTRYPOINT ["/usr/local/bin/lua", "updatesecret.lua"]
CMD ["--config-file", "config.lua"]

# updatesecret
## description
this app aims to install secret stored in vault on the system.

An example of configuration file is stored in the share folder

Currently only the secret storage engine "token" is available

## pre-requisite
This code needs the following lua packages:
* luasec
* luafilesystem
* luajson

## installation
```
cd $SRC
make remove
```

## remove application
```
cd $SRC
make remove
```

## configuration
### generic parameters
* ```vault_url``` url of the API vault server.
* ```vault_ca``` certificate authority
* ```vault_access``` type of the authentication engine. Only 'token' is supported
* ```vault_token_file``` path of the file containing the token
* ```secret_type``` type of the secret enging to download. Only "pki" is currently supported
* ```post_action```: array of commands to execute after having installed the secret

### secret engine PKI
* ```pki_path``` path in vault.
* ```pki_role``` PKI role in vault
* ```pki_common_name``` common_name to use in the certificate

* ```tls_cert_private``` full path of the file which contains the newly generated private key
* ```tls_cert_public``` full path of the file which contains the generated public key 
* ```tls_cert_ca``` full path of the file which contains the certificate authority of tls_cert_public
* ```tls_file_owner``` owner of the tls_cert_private, tls_cert_public and tls_cert_ca files
* ```tls_file_group``` group of the tls_cert_private, tls_cert_public and tls_cert_ca files

### Example
```
CONFIG={
  token={
    vault_url='https://vault.mydomain.ch:8200/v1',
    vault_ca='/etc/ssl/vault/ca.crt',
    vault_access="token",
    vault_token_file="/tmp/refresh.token",

    secret_type='pki',
    pki_path='pki',
    pki_role='rolePKI',
    pki_common_name='my.server.example.ch',

    tls_cert_private='/etc/ssl/private/my.key',
    tls_cert_public='/etc/ssl/cert/my.crt',
    tls_cert_ca='/etc/ssl/ca/my.ca',
    tls_file_owner='appuser',
    tls_file_group='appgroup',

    post_action={
      "systemctl stop myapp",
      "systemctl start myapp",
    }
  }
}

```

## usage
### one-liner
* ``` updatesecret --config-file <config>```
* ``` updatesecret --config-directory <config_dir>```
### docker
* ``` docker run --rm -v etc/config.lua:config.lua ybovard:updatesecret:latest```
* ``` docker run --rm -v etc/config.lua:my_config.lua ybovard:updatesecret:latest --config-file my_config.lua```
* ``` docker run --rm -v etc:/etc/updatesecret ybovard:updatesecret:latest --config-directory /etc/updatesecret```


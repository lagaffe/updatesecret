local name = ""
local successList={}
local failureList={}
local iErr = 0
local iOk = 0

local function startTest(testName)
    name = testName
    successList={}
    failureList={}
    iFail = 0
    iOk = 0
end

local function addSuccess(label)
    iOk = iOk + 1
    successList[iOk]=label
end

local function addFailure(descr)
    iErr = iErr + 1
    failureList[iErr] = descr
end


local function summary()
    io.write("# run test ", name, "\n")
    io.write("## ", iOk, " success", "\n")
    io.write("## ", iErr, " failure(s)", "\n")
    for key, val in ipairs(failureList) do
        io.write("* ", val, "\n")
    end
end

local function rc()
    if iErr == 0 then
        return 0
    else
        return 1
    end
end

local function expectError(label, expectedCode, obj, errCode)
    errNum=0

    if errCode ~= expectedErrCode then
        addFailure(label .. ": expected '" .. expectedCode .. " but recieved '" .. errCode .. "'")
        errNum=errNum + 1
    end

    if obj then
        addFailure(label .. ": object returned should be False or nil but '" .. obj .. "' recieved")
        errNum=errNum + 1
    end

    if errNum == 0 then
        addSuccess(label)
    end
end

local function expectFunction(label, fct)
    if fct == nil then
        addFailure(label .. ": function expected but nil recieved")
    else
        addSuccess(label)
    end
end

local function expectValue(label, val, expected)
    if val == nil then
        addFailure(label .. ": value expected but nil recieved")
    elseif val ~= expected then
        addFailure(label .. ": expected '" .. expected .. "' but '" .. val .. "' recieved")
    else
        addSuccess(label)
    end
end

local function expectBool(label, val, expected, errmsg)
    errTxt=''
    if errmsg then
      errTxt="(error recieved: '" .. errmsg .."')"
    end
    if val == nil then
        addFailure(label .. ": value expected but nil recieved " .. errTxt )
    elseif val ~= expected then
        if expected then
            addFailure(label .. ": expected True but False recieved " .. errTxt)
	else
            addFailure(label .. ": expected False but True recieved " .. errTxt)
	end
    else
        addSuccess(label)
    end
end

local function expectNil(label, val)
    if val == nil then
        addSuccess(label)
    else
        addFailure(label .. ": expected nil value but '"..val.."' recieved")
    end
end

local function expectNotNil(label, val)
    if val ~= nil then
        addSuccess(label)
    else
        addFailure(label .. ": recieved nil but object expected")
    end
end

local function expectTabLength(label, tab, expectedLen)
    if tab == nil then
        addFailure(label .. ": array expected but nil recieved")
    elseif #tab ~= expectedLen then
        addFailure(label .. ": array of " .. expectedLen .. " elements expected but " .. #tab .. " elements found")
    else
        addSuccess(label)
    end
end

return {
    startTest = startTest,
    summary = summary,
    rc = rc,
    expectError = expectError,
    expectFunction = expectFunction,
    expectNil = expectNil,
    expectNotNil = expectNotNil,
    expectBool = expectBool,
    expectValue = expectValue,
    expectTabLength = expectTabLength,
}

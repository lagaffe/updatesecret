CONFIG={
  token={
    vault_url='https://vault.intern.mydomain.ch:8200/v1',
    vault_ca='/etc/ssl/vault/ca.crt',
    vault_access="token",
    vault_token_file="/tmp/refresh.token",

    secret_type='pki',
    pki_path='pki',
    pki_role='rolePKI',
    pki_common_name='my.server.example.ch',

    tls_cert_private='/etc/ssl/private/my.key',
    tls_cert_public='/etc/ssl/cert/my.crt',
    tls_cert_ca='/etc/ssl/ca/my.ca',
    tls_file_owner='appuser',
    tls_file_group='appgroup',

    post_action={
      "systemctl stop myapp",
      "systemctl start myapp",
    }
  }
}

http = require "ssl.https"
ltn12 = require("ltn12")
json = require("json")
os = require("os")
io = require "io"

local token = nil
local secret = nil
local cfgName = nil
local cfg = { }
local errmsg = nil

local function configure (label, config) 
  cfgName=label
  cfg=config
  token=nil
  errmsg=nil
end

local function loginToken()
  local stdin=io.input()
  local stdout=io.output()
  local refresh_token=nil

  io.input(cfg['vault_token_file'])
  refresh_token=io.read()
  io.input(stdin)
  

  if cfg.vault_access ~= "token" then return nil end
  local body_query=json.encode({increment="1h"})
  local resp={}
  local res, code, resp_headers, status = http.request{
    url=cfg['vault_url'] .. '/auth/token/renew-self';
    method='POST',
    headers={
      ["Content-Type"] = "application/json";
      ["Content-Length"] = #body_query;
      ["X-Vault-Token"] = refresh_token;
    },
    cafile=cfg['vault_ca'],
    source = ltn12.source.string(body_query),
    sink = ltn12.sink.table(resp),
  }
--[[
  print("try token")
  print("* code: "..code)
  print("* status: "..status)
  print("* res: " .. res)
  print("* resp_headers")
  for k,v in pairs(resp_headers) do
    print(k .. "=>"..v)
  end
  print("* resp: ")
  for k,v in pairs(resp) do
    print(k .. "=>"..v)
  end
--]]
  if code == 200 then
    errmsg=nil
    refresh_token = json.decode(resp[res])['auth']['client_token']
    io.output(cfg['vault_token_file'])
    io.write(refresh_token)
    io.output(stdout)
    return refresh_token
  else
    errmsg=status
    return nil
  end
end

local function loginAppRole()
  if cfg.vault_access ~= "appRole" then return nil end
  local tk='coucou'
--[[
  print("try loginAppRole")
  local body_query=json.encode({role_id=cfg['vault_role_id'], secret_id=cfg['vault_secret_id']})
  local resp={}
  local params = {
    protocol = "tlsv1",
    verify = "none",
    options = "all",
  }
  local res, code, resp_headers, status = http.request{
    url=cfg['vault_url'] .. '/auth/approle/login';
    method='POST',
    headers={
      ["Content-Type"] = "application/json";
      ["Content-Length"] = #body_query;
    },
    cafile=cfg['vault_ca'],
    source = ltn12.source.string(body_query),
    sink = ltn12.sink.table(resp),
  }
  print(res)
  print(code)
  print(resp_headers)
  print(status)
--]]
  return tk
end

local function login()
  if not token then token=loginAppRole() end
  if not token then token=loginToken() end
end

local function getSecretPKI()
  if cfg['secret_type'] ~= 'pki' then return end

  local body_query=json.encode({common_name=cfg['pki_common_name']})
  local resp={}
  local res, code, resp_headers, status = http.request{
    url=cfg['vault_url'] .. '/'..cfg['pki_path']..'/issue/'..cfg['pki_role'];
    method='POST',
    headers={
      ["Content-Type"] = "application/json";
      ["Content-Length"] = #body_query;
      ["X-Vault-Token"] = token;
    },
    cafile=cfg['vault_ca'],
    source = ltn12.source.string(body_query),
    sink = ltn12.sink.table(resp),
  }
  if code == 200 then
    data=json.decode(resp[res])['data']
    -- for k,v in pairs(data) do print(k .. "=>"..v) end
    -- for k,v in pairs(data) do print(k ) end
    errmsg=nil
    ret={}
    ret.ca = data['issuing_ca']
    ret.public = data['certificate']
    ret.private = data['private_key']
    --for k,v in pairs(ret) do print(k .. "=>"..v) end
    return ret
  else
    errmsg=status
  end
end

local function getSecret()
  if not secret then secret=getSecretPKI() end
  -- for k,v in pairs(secret) do print(k .. "=>"..v) end
end

local function installSecretPKI()
  if cfg['secret_type'] ~= 'pki' then return end

  local ptr=nil

  ptr=assert(io.open(cfg['tls_cert_public'],"w"))
  ptr:write(secret['public'])
  ptr:close()
  os.execute("chown " .. cfg['tls_file_owner'] .. ":" .. cfg['tls_file_group'] .. " " .. cfg['tls_cert_public'])

  ptr=assert(io.open(cfg['tls_cert_private'],"w"))
  ptr:write(secret['private'])
  ptr:close()
  os.execute("chown " .. cfg['tls_file_owner'] .. ":" .. cfg['tls_file_group'] .. " " .. cfg['tls_cert_private'])
  os.execute("chmod 400 " .. cfg['tls_cert_private'])

  ptr=assert(io.open(cfg['tls_cert_ca'],"w"))
  ptr:write(secret['ca'])
  ptr:close()
  os.execute("chown " .. cfg['tls_file_owner'] .. ":" .. cfg['tls_file_group'] .. " " .. cfg['tls_cert_ca'])
end

local function executePostCommand()
  if cfg["post_action"] == nil then return end
  for lb, cmd in pairs(cfg['post_action']) do
    os.execute(cmd)
  end
end

local function installSecret()
  installSecretPKI()
  -- val=get_from_vault
  -- if type == pki
  --   extract/install private certificate
  --   extract/install public  certificate
  --   extract/install ca certificates chain
  -- 
  -- if service is defined:
  --   stop service
  --   start service
  executePostCommand()
end

local function reset()
  cfgName = nil
  cfg = { }
  token = nil
  secret = nil
  errmsg = nil
end

return {
  errorMessage = errmsg,
  configure = configure,
  loginToken = loginToken,
  login = login,
  isLogged = function () return token ~= nil end,
  reset = reset,
  getSecret = getSecret,
  installSecret = installSecret,
  secret = function () return secret end
}

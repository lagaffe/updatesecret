#!/usr/bin/env lua

libus = require "libUS"
lfs = require "lfs"

-- argument parsing
usageMessage="Usage: " .. arg[0] .. " --config-file <config> OR " .. arg[0] .. " --config-dir <config_directory>"
assert(#arg == 2, usageMessage)
assert(arg[1]=='--config-file' or arg[1] == '--config-dir', usageMessage)

if(arg[1] == '--config-file') then
  dofile(arg[2])
elseif(arg[1] == '--config-dir') then
  CONFIG={}
  for file in lfs.dir(arg[2]) do
    if string.sub(file,-4) == '.lua' then
      dofile(arg[2] .. "/" .. file)
    end
  end
end

for secret_cfg in pairs(CONFIG) do
  libus.configure(secret_cfg, CONFIG[secret_cfg])
  libus.login()
  assert(libus.isLogged(),"not authenticated")
  libus.getSecret()
  libus.installSecret()
  libus.reset()
end

return 0

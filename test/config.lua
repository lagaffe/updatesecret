CONFIG={
  approle={
    vault_url='https://vault.intern.ybovard.ch:8200/v1',
    vault_ca='/home/yves_yvb/updatesecret/NOT_VERSIONNED/vault.crt',
    vault_access='appRole',
    vault_role_id="5655d519-6a68-d48b-dd62-c19a1c1b4edb",
    vault_secret_id="40959142-8cca-154b-f898-e1a157b9ec08",

    secret_type='pki',
    secret_path='monitoringCA',
    tls_cert_private='/etc/ssl/traefik/private.key',
    tls_cert_public='/etc/ssl/traefik/ssl.crt',
    tls_cert_ca='/etc/ssl/traefik/ca.crt',
    tls_file_owner='traefik',
    tls_file_group='traefik',

    restart_service='traefik'
  },
  token={
    vault_url='https://vault.intern.mydomain.ch:8200/v1',
    vault_ca='/etc/ssl/vault/ca.crt',
    vault_access="token",
    vault_token="s.RvSHmYNeD3583YMAKsqtyIa3",

    secret_type='pki',
    pki_path='pki',
    pki_role='rolePKI',

    tls_cert_private='/etc/ssl/private/my.key',
    tls_cert_public='/etc/ssl/cert/my.crt',
    tls_cert_ca='/etc/ssl/ca/my.ca',
    tls_file_owner='appuser',
    tls_file_group='appgroup',

    post_action={
      "systemctl stop myapp",
      "systemctl start myapp",
    }
  }
}

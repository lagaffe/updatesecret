package.path="../src/?.lua;" .. package.path
package.path="../lib/?.lua;" .. package.path

libus = require "libUS"
TestFCT = require "testfunctions"
dofile('config.lua')


TestFCT.startTest("login funtions")
for secret_cfg in pairs(CONFIG) do
  libus.configure(secret_cfg, CONFIG[secret_cfg])
  libus.login()
  TestFCT.expectBool("is logged with "..secret_cfg, libus.isLogged(), true,libus.errorMessage)
  libus.reset()
  TestFCT.expectBool("is not logged with "..secret_cfg.." after reset", libus.isLogged(), false)
end
TestFCT.summary()

TestFCT.startTest("get PKI certificates")
libus.configure(secret_cfg, CONFIG['token'])
libus.login()
libus.getSecret()
TestFCT.expectBool("has got a secret", libus.getSecret()==nil, true)
TestFCT.expectNil("error message", libus.errorMessage)
libus.installSecret()
TestFCT.summary()

if TestFCT.rc() ~= 0 then os.exit(TestFCT.rc()) end
os.exit(0)
